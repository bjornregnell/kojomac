echo "*** Copying Kojo2 (should be installed in ~/Kojo2)"
cp -a ~/Kojo2 . 
echo "*** Creating icon set"
iconutil -c icns kojo.iconset
echo "*** runnning ant with jarbuilder"
ant -lib .
echo "*** removing old Kojo.app.dmg if present"
rm -f Kojo.app.dmg
echo "*** Creating Kojo.app.dmg ... this will take some time ..."
hdiutil create Kojo.app.dmg -volname "Kojo" -srcfolder Kojo.app
echo "*** READY ***"

