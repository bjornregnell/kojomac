#!/bin/bash
read -p "Enter version number e.g. 2.4.08 : " -e ver
file=Kojo-$ver.app.dmg
host=web.cs.lth.se
path="/Websites/Fileadmin/kojo"
echo Uploading $file to $host
read -p "Username: " -e uname
echo scp $file $uname@$host:$path
scp $file $uname@$host:$path

