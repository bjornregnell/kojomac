# README #

Repo with files to build [Kojo](http://www.kogics.net/kojo) for mac.

### How to build Kojo.app for mac? ###

On a MacOSX box:

Preparations needed once:

0. Install [Java JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
0. Install [ant](http://ant.apache.org) 
0. Clone or [Download](https://bitbucket.org/bjornregnell/kojomac/downloads) this repo into ``~/kojomac``

For each release:

0. Remove old ``~/Kojo2`` if any.
0. Install the latest version of [kojo](http://www.kogics.net/kojo-download) that you want to build into ``~/Kojo2``
    * This can be done using ``source ~/kojomac/downloadAndInstallKojo.sh``
    * PLEASE NOTE: you need to explicit change the destination folder to ``~/Kojo2`` during interactive installation.
0. ``cd ~/kojomac``
0. Run ``./build.sh`` that will: create icons, run ant and create a Kojo.app.dmg file. It may take some time...
0. Rename the Kojo.app.dmg to Kojo-$version.app.dmg e.g. ``mv Kojo.app.dmg Kojo-2.4.03.app.dmg``
0. Upload the file, e.g. using ``source upload.sh``