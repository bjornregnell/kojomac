#!/bin/bash
read -p "Enter version number e.g. 2.4.03 : " -e ver
file=kojoInstall-$ver.jar
site=https://bitbucket.org/lalit_pant/kojo/downloads/$file
dest=~/Downloads/$file
echo Downloading Kojo installer from $site
curl --fail -L -o $dest $site
java -jar $dest
